using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PyGAP2019
{
   class GAPclass
   {  public int customersNumber;  // numero clienti
      public int storesNumber;  // numero magazzini
      public double[,] assignmentCosts;  // costi assegnamento
      public int[] customersRequests;    // richieste clienti
      public int[] storesCapacity;    // capacità magazzini

      public int[] sol,solbest;    // per ogni cliente, il suo magazzino
      public double zub,zlb;

      const double EPS = 0.0001;
      System.Random rnd = new Random(550);

      public GAPclass()
      {  zub = double.MaxValue;
         zlb = double.MinValue;
      }

      // costruzione, ognuno al suo deposito più vicino
      public double simpleContruct()
      {  int i,ii,j;
         int[] capleft = new int[storesCapacity.Length];
         int[] ind = new int[storesNumber];
         double[] distances = new double[storesNumber];
         Array.Copy(storesCapacity,capleft,storesCapacity.Length);

         zub = 0;
         for(j=0;j<customersNumber;j++)
         {  for(i=0;i<storesNumber;i++)
            {  distances[i]= assignmentCosts[i,j]; // Distanza di un customer (j) da ogni magazzino (i)
               ind[i] = i;
            }
            Array.Sort(distances,ind);
            ii=0;
            while(ii<storesNumber)
            {  i=ind[ii];
               if(capleft[i]>=customersRequests[j])
               {  sol[j]=i;
                  capleft[i] -= customersRequests[j];
                  zub += assignmentCosts[i, j];
                  break;
               }
               ii++;
            }
            if(ii==storesNumber)
               Trace.WriteLine("[SimpleConstruct] ii="+ii);
         }
         return zub;
      }

      // tolgo un cliente da un magazzino e lo metto in un altro
      // Opt10 si blocca nella ricerca degli ottimi locali
      public double opt10(double[,] costs)
      {  double z=0;
         int i, isol, j;
         int[] capres = new int[storesCapacity.Length];

         Array.Copy(storesCapacity, capres, storesCapacity.Length);
         for (j = 0; j < customersNumber; j++)
         {  capres[sol[j]] -= customersRequests[j];
            z += costs[sol[j],j];
         }

l0:      for (j = 0; j < customersNumber; j++)
         {
            isol = sol[j];
            for (i = 0; i < storesNumber; i++)
            {
               if (i == isol) continue;
               if (costs[i, j] < costs[isol, j] && capres[i] >= customersRequests[j])
               {
                  sol[j] = i;
                  capres[i] -= customersRequests[j];
                  capres[isol] += customersRequests[j];
                  // z, costo  
                  z -= (costs[isol, j] - costs[i, j]);
                  if(z<zub)
                  {  zub = z;
                     Trace.WriteLine("[1-0 opt] new zub " + zub);
                  }
                  goto l0;
               }
            }
         }
         double zcheck = 0;
         for (j = 0; j < customersNumber; j++)
            zcheck += costs[sol[j], j];
         if (Math.Abs(zcheck - z) > EPS)
            System.Windows.Forms.MessageBox.Show("[1.0opt]");
         return z;
      }

      // Tabu search
      // Ttenure -> Quanti cicli una soluzione è in tabu
      // maxiter -> quante volte, al massimo, itero tabu search
      public double TabuSearch(int Ttenure, int maxiter)
      {  int i,isol,j,imax,jmax,iter;
         double z,DeltaMax;
         int[] capres = new int[storesCapacity.Length];
         int[,] TL=new Int32[storesNumber,customersNumber];
            /*
            1.	Generate an initial feasible solution S, 
	            set S* = S and initialize TL=nil.
            2.	Find S' \in N(S), such that 
	            z(S')=min {z(S^), foreach S^\in N(S), S^\notin TL}.
            3.	S=S', TL=TL U {S}, if (z(S*) > z(S)) set S* = S.
            4.	If not(end condition) go to step 2.
            */

         Array.Copy(storesCapacity,capres,storesCapacity.Length);
         for(j=0;j<customersNumber;j++)
            capres[sol[j]] -= customersRequests[j];

         z = zub;
         iter=0;
         for(i=0;i<storesNumber;i++)
            for(j=0;j<customersNumber;j++)
               TL[i,j]=int.MinValue;

         Trace.WriteLine("Starting tabu search");
         start: DeltaMax=imax=jmax=int.MinValue;
         iter++;

         for(j=0;j<customersNumber;j++)
         {  isol = sol[j];
            for(i=0;i<storesNumber;i++)
            {  if(i==isol) continue;
               if( (assignmentCosts[isol,j] - assignmentCosts[i,j]) > DeltaMax && capres[i] >= customersRequests[j]  && (TL[i,j]+Ttenure)<iter)
               {  imax = i;
                  jmax = j;
                  DeltaMax = assignmentCosts[isol,j] - assignmentCosts[i,j];
               }
            }
         }

         isol = sol[jmax];
         sol[jmax] = imax;
         capres[imax] -= customersRequests[jmax];
         capres[isol] += customersRequests[jmax];
         z -= DeltaMax;
         if(z < zub)
            zub = z;
         TL[imax,jmax]=iter;
         Trace.WriteLine("[Tabu Search]  z "+z+" ite "+iter+" deltamax "+DeltaMax);

         if(iter<maxiter)          // end condition
            goto start;
         else
            Trace.WriteLine("Tabu search: fine");

         double zcheck = 0;
         for(j=0;j<customersNumber;j++)
            zcheck += assignmentCosts[sol[j],j];
         if(Math.Abs(zcheck - z) > EPS)
            System.Windows.Forms.MessageBox.Show("[tabu search]");
         return zub;
      }

      // ILS, su 1-0
      public double IteratedLocalSearch(int maxIter)
      {  int iter;
         double z;

         iter = 0;
         while(iter<maxIter)
         {  z = opt10(assignmentCosts);
            dataPerturbation();
            iter++;
         }
         return zub;
      }

      private void dataPerturbation()
      {  int i,j;
         double[,] cPert = new double[storesNumber,customersNumber];

         for(i=0;i<storesNumber;i++)
            for(j=0;j<customersNumber;j++)
               cPert[i,j] = assignmentCosts[i,j]+assignmentCosts[i,j]*0.5*rnd.NextDouble();

         opt10(cPert);
      }

      // controllo ammissibilità soluzione
      public double checkSol(int[] sol)
      {  double cost=0;
         int j;
         int[] capused = new int[storesNumber];

         // controllo assegnamenti
         for(j=0;j<customersNumber;j++)
            if(sol[j]<0 || sol[j]>=storesNumber)
            {  cost = double.MaxValue;
               goto lend;
            }
            else
               cost += assignmentCosts[sol[j],j];

         // controllo capacità
         for(j=0;j<customersNumber;j++)
         {  capused[sol[j]] += customersRequests[j];
            if(capused[sol[j]] > storesCapacity[sol[j]])
            {  cost = double.MaxValue;
               goto lend;
            }
         }

         lend:    return cost;
      }
   }
}
