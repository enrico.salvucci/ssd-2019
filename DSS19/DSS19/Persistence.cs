﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.IO;
using PyGAP2019;

namespace DSS19
{
    class Persistence //model
    {
        public string connectionString;
        private string factory;
        private GAPclass G;


        public Persistence(string connString, string f)
        {
            connectionString = connString;
            factory = f;
            G = new GAPclass();

        }

        public string readAllCustomers(string dbpath)
        {
            List<string> lstClienti;
            string ret = "Error reading DB";
            try
            {
                using (var ctx = new SQLiteDatabaseContext(dbpath))
                {
                    lstClienti = ctx.Database.SqlQuery<string>("SELECT distinct customer from ordini").ToList();
                }

                List<string> lstOutStrings = new List<string>();

                lstClienti.ForEach(c => lstOutStrings.Add("'" + c + "'"));
                ret = string.Join(",", lstOutStrings);

            }
            catch (Exception ex)
            {
                Trace.WriteLine($"Error: {ex.Message}");
            }

            return ret;
        }

        // legge una stringa con i codici clienti da graficare
        public string readSingleCustomer(string dbpath)
        {
         string result = "Error reading DB";
         try
         {
            using (var ctx = new SQLiteDatabaseContext(dbpath))
            {
                    result = ctx.Database.SqlQuery<string>("SELECT distinct customer from ordini ORDER BY RANDOM() LIMIT 1").SingleOrDefault();     
            }

            result = "'" + result + "'";            
            }
            catch (Exception ex)
            {
            Trace.WriteLine($"Error: {ex.Message}");
            }

            return result;
      }

        public Boolean previsionFileExists()
        {
            return File.Exists("GAPreq.dat");
        }

        public void writePrevisionOnFile(double[] prevision)
        {
            G.customersRequests = Array.ConvertAll<double, int>(prevision, new Converter<double, int>(i => System.Convert.ToInt32(i)));
            File.WriteAllLines("GAPreq.dat", G.customersRequests.Select(x => x.ToString()));
        }

        public void setPrevisionFromFile()
        {
            string[] txtData = File.ReadAllLines("GAPreq.dat");
            G.customersRequests = Array.ConvertAll<string, int>(txtData, new Converter<string, int>(i => int.Parse(i)));
        }

        public double getSimpleConstructValue()
        {
            return G.simpleContruct();
        }

        public double getOpt10Value()
        {
            return G.opt10(G.assignmentCosts);
        }

        public double getTabuSearchValue()
        {
            return G.TabuSearch(30, 100);
        }

        public string readQuantitiesListORM(string dbpath)
        {

            List<int> lstQuantities;
            string ret = "Error reading DB";
            try
            {
                using (var ctx = new SQLiteDatabaseContext(dbpath))
                {
                    lstQuantities = ctx.Database.SqlQuery<int>("SELECT quant from ordini").ToList();
                }

                ret = string.Join("\n", lstQuantities);
                Trace.WriteLine(ret);
            }
            catch (Exception ex)
            {
                Trace.WriteLine($"Error: {ex.Message}");
            }

            return ret;
        }

        // Reads an instance from the db
        public void readGAPinstance(string dbOrdinipath)
        {
            int i, j;
            List<int> lstCap;
            List<double> lstCosts;

            try
            {
                using (var ctx = new SQLiteDatabaseContext(dbOrdinipath))
                {
                    lstCap = ctx.Database.SqlQuery<int>("SELECT cap from capacita").ToList();
                    G.storesNumber = lstCap.Count();
                    G.storesCapacity = new int[G.storesNumber];
                    for (i = 0; i < G.storesNumber; i++)
                        G.storesCapacity[i] = lstCap[i];

                    lstCosts = ctx.Database.SqlQuery<double>("SELECT cost from costi").ToList();
                    G.customersNumber = lstCosts.Count / G.storesNumber;
                    G.assignmentCosts = new double[G.storesNumber, G.customersNumber];
                    G.customersRequests = new int[G.customersNumber];
                    G.sol = new int[G.customersNumber];
                    G.solbest = new int[G.customersNumber];
                    G.zub = Double.MaxValue;
                    G.zlb = Double.MinValue;

                    for (i = 0; i < G.storesNumber; i++)
                        for (j = 0; j < G.customersNumber; j++)
                            G.assignmentCosts[i, j] = lstCosts[i * G.customersNumber + j];

                    for (j = 0; j < G.customersNumber; j++)
                        G.customersRequests[j] = -1;          // placeholder
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine("[readGAPinstance] Error:" + ex.Message);
            }

            Trace.WriteLine("Fine lettura dati istanza GAP");
        }

    }
         
}
