﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Configuration;
using PyGAP2019;
using System.Drawing;
using System.IO;

namespace DSS19
{

    class Controller
    {
        private Persistence P;
        private string dbPath;
        private string pythonPath;
        private string pythonScriptPath;
        private PythonRunner pyRunner;

        public Controller(string _pyPath, string _pyScriptPath)
        {
            this.pythonPath = _pyPath;
            this.pythonScriptPath = _pyScriptPath;
    
            P = new Persistence(this.pythonPath, this.pythonScriptPath);
            this.pyRunner = new PythonRunner(pythonPath, 20000);
        }

        public string readAllCustomers(string dbOrdiniPath)
        {
            return P.readAllCustomers(dbOrdiniPath);
        }

        public string readSingleCustomer(string dbOrdiniPath)
        {
            return P.readSingleCustomer(dbOrdiniPath);
        }

        public void readQuantitiesListORM()
        {
            P.readQuantitiesListORM(dbPath);
        }

        public async Task<string> readCustomersOrdersPrevision(string dbOrdiniPath, string pythonScript, string customer)
        {

            // 
            //  Un solo customer!!!
            //
            string fcast = "";

            Trace.WriteLine("Getting the orders chart (prevision) ...");
            pythonScriptPath = System.IO.Path.GetFullPath(pythonScriptPath);
            try
            {
                string list = await pyRunner.getStringsAsync(
                    this.pythonScriptPath,
                    pythonScript,
                    this.pythonScriptPath,
                    dbOrdiniPath,
                    customer);

                string[] lines = list.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string s in lines)
                {
                    if (s.StartsWith("Actual"))
                    {
                        fcast = fcast + Environment.NewLine + s;
                    }
                }

                return fcast;
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[ReadCustomerOrdersChart]: {e.ToString()}");
                return null;
            }
        }

        public async Task<double[]> getPrevisionsList(string dbOrdiniPath, string pythonScript)
        {
            string customers = readAllCustomers(dbOrdiniPath);
            char[] spearator = { ','};

            string[] customersList = customers.Split(spearator, StringSplitOptions.RemoveEmptyEntries);

            List<double> previsions = new List<double>();
            foreach (string customer in customersList) {
                double prevision = await readLastCustomerOrdersPrevision(dbOrdiniPath, pythonScript, customer);
                previsions.Add(prevision);
                Trace.WriteLine(customer);
                Trace.WriteLine(prevision);
            }

            return previsions.ToArray();
        }

        public async Task<double> readLastCustomerOrdersPrevision(string dbOrdiniPath, string pythonScript, string customer)
        {

            // 
            //  Un solo customer!!!
            //
            string fcast = "";

            pythonScriptPath = System.IO.Path.GetFullPath(pythonScriptPath);
            try
            {
                string list = await pyRunner.getStringsAsync(
                    this.pythonScriptPath,
                    pythonScript,
                    this.pythonScriptPath,
                    dbOrdiniPath,
                    customer);

                string[] lines = list.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string s in lines)
                {
                    if (s.StartsWith("Actual"))
                    {
                        fcast = fcast + Environment.NewLine + s;
                    }
                }

                int from = fcast.Length - 5;
                int to = fcast.Length;
                string result = fcast.Substring(from, to - from);
                return Math.Round(double.Parse(result));
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[ReadCustomerOrdersChart]: {e.ToString()}");
                return 0;
            }
        }

        public async Task<Bitmap> readCustomerOrdersChart(string dbOrdiniPath, string pythonScript, string customers)
        {

            Trace.WriteLine("Getting the orders chart ...");

            pythonScriptPath = System.IO.Path.GetFullPath(pythonScriptPath);
            try
            {
                Bitmap bmp = await this.pyRunner.getImageAsync(
                    this.pythonScriptPath,
                    pythonScript,
                    this.pythonScriptPath,
                    dbOrdiniPath,
                    customers);
                return bmp;
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[ReadCustomerOrdersChart]: {e.ToString()}");
                return null;
            }
        }

        public async void optimizeGAP(string dbOrdiniPath)
        {
            
            if(P.previsionFileExists())
            {
                P.setPrevisionFromFile();
            } else {
                double[] list = await getPrevisionsList(dbOrdiniPath, "arima_forecast.py");
                P.writePrevisionOnFile(list);
            }

            P.readGAPinstance(dbOrdiniPath);

            double zub = P.getSimpleConstructValue();
            Trace.WriteLine($"Constructive, zub = {zub}");
            // zub = G.opt10(G.c);
            zub = P.getOpt10Value();
            Trace.WriteLine($"opt10 search, zub = {zub}");
            // zub = G.TabuSearch(30, 100);
            zub = P.getTabuSearchValue();
            Trace.WriteLine($"TabuSearch search, zub = {zub}");
        }
    }
}
