﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Configuration;

namespace DSS19
{

    public partial class App : Form
    {

        Controller C;
        TextBoxTraceListener _textBoxListener;
        private string dbOrdiniPath;
        private string pythonPath;
        private string pythonScriptPath;
        private Bitmap bmp;
       

        public App()
        {
            InitializeComponent();
            _textBoxListener = new TextBoxTraceListener(txtConsole);
            Trace.Listeners.Add(_textBoxListener);

            dbOrdiniPath = ConfigurationManager.AppSettings["dbordiniFile"];
            pythonPath = ConfigurationManager.AppSettings["pythonPath"];
            pythonScriptPath = ConfigurationManager.AppSettings["pyScripts"];

            C = new Controller(this.pythonPath, this.pythonScriptPath);
        }

        private async void showOrdersTimeSeries()
        {
            string customers = C.readAllCustomers(dbOrdiniPath);
            bmp = await C.readCustomerOrdersChart(dbOrdiniPath, "chartOrders.py", customers);
            pictureBox2.Image = bmp;
        }

        private async void loadArima()
        {

            string customer = C.readSingleCustomer(dbOrdiniPath);
            Trace.WriteLine(customer);

            bmp = await C.readCustomerOrdersChart(dbOrdiniPath, "arima_forecast.py", customer);
            pictureBox2.Image = bmp;

            string prevision = await C.readCustomersOrdersPrevision(dbOrdiniPath, "arima_forecast.py", customer);
            Trace.WriteLine(prevision);


        }

        private async void readPrevision()
        {
            string customers = C.readAllCustomers(dbOrdiniPath);
            string[] customersArray = customers.Split(',');

            foreach(string s in customersArray)
            {
                double prevision = await C.readLastCustomerOrdersPrevision(dbOrdiniPath, "arima_forecast.py", s);
                Trace.WriteLine(prevision.ToString());
            }
        }

        private void getOptimization()
        {
            C.optimizeGAP(dbOrdiniPath);
        }

        private void readButton_Click(object sender, EventArgs e)
        {
            showOrdersTimeSeries();
        }

        private void btnSARIMA_Click(object sender, EventArgs e)
        {
            loadArima();
        }

        private void cleanTextBox_Click(object sender, EventArgs e)
        {
            txtConsole.Text = String.Empty;
        }

        private void txtConsole_TextChanged_1(object sender, EventArgs e)
        {}


        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {}

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            readPrevision();            
        }

        private void optimizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            getOptimization();
        }
    }
}
