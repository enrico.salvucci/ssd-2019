# Python

## Anaconda environment
Anaconda Prompt mostra, tra parentesi all'inizio della riga di comando, l'environment attivo.

Da riga di comando (Anaconda Prompt) possiamo creare ambienti virtuali.
Possiamo impostare environment diversi in modo diverso. Per esempio, se ci serve una qualche libreria che utilizza una specifica versione di python, possiamo installare nell'environment una versione di python piuttosto che un'altra.

In generale non lavoriamo su "base" (environment di default) ma su ambiente ad-hoc. Useremo tensorflow che richiede python 3.6.8

> conda create -n cplex python=3.6.8

con -n dico il nome dell'ambiente che stò creando (in questo esempio "cplex")
poi ho detto di installare la versione 3.6.8 di python in nell'environment "cplex"

cplex è una libreria che potremmo, volendo, usare per l'esame per aumentare il 27

> conda info -envs

mostra gli environments disponibili

> pip

comando che permette di scaricare e installare librerie (alternativa più potente di conda, c'è più roba)

> conda activate cplex

Attivo l'environment "cplex" che avevo creato prima

In Anaconda navigator -> "Application on" posso scegliere l'environment che voglio tra quelli disponibili (per esempio base o cplex, adesso) 

## Spyder

l'icona con la freccia nera e un cursore sopra mette in esecuzione la riga di codice sulla quale ci troviamo. Se selezioniamo più righe la freccia nera con il cursore sopra esegue solo le righe selezionate.

Il triangolo verde esegue tutto il file .py (python) dopo che questo è stato salvato.

Run -> Configuration per file (configuriamo come vogliamo eseguire il nostro file python)

Sarà utile il pannello Variables explorer per vedere i valori delle variabili

## Python (Linguaggio)
In python non ci sono gli array. Alcune librerie però hanno una loro implementazione per gli array.In python ci sono le liste.
Nelle liste posso avere tipi di dato diversi (int/string/boolean/ecc...).
Si rappresentano con le parentesi quadre

Tuple -> come le liste ma read-only. Si rappresentano con le parentesi tonde

Dictionary -> Insieme di coppie attributo-valore

Slicing -> Operazione fatta sulle liste per "tagliarle" da un indice ad un altro

> list = [0, 1 , 2, 3, 4, 5, 6, 7, 8, 9]

> list[3:7]

[3, 4, 5, 6] # Ho tagliato la lista a partire dal terzo elemento fino al settimo