# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 16:39:10 2019

@author: Enrico
"""

# Sliding window MLP, Airline Passengers dataset (predicts t+1)
import yaml
import os, math
import numpy as np, pandas as pd
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential 
from tensorflow.keras.layers import Dense

# Funzione che verrrà chiamata sul TS della serie e la trasformerà in insieme
# di record della matrice che stò costruendo
# Crea le sliding windows
# npast = 1 dice quanti valori nel passato userò per fare la previsione nel futuro
# from series of values to windows matrix
def compute_windows(nparray, npast=1):
    dataX, dataY = [], []  # window and value
    
    # Per tutti i sottoinsioni (windows) identifica la posizione di partenza 
    # "appendo" a X il valore a, "appendo" a Y il valore immediatamente successivo
    # Ogni window sarà lunga npast+ 1
    for i in range(len(nparray)-npast-1):
        a = nparray[i:(i+npast), 0]
        dataX.append(a)
        dataY.append(nparray[i + npast, 0])
        return np.array(dataX), np.array(dataY)
    
np.random.seed(550) # for reproducibility

with open("../../config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.Loader)

os.chdir(r''+cfg['path'])

df = pd.read_csv('./serie_storiche/rawAirlinesPassengers.csv', usecols=[0],
                 names=['value'], header=0)
dataset = df.values # time series values
dataset = dataset.astype('float32')  # needed for MLP input

# train - test sets
cutpoint  = int(len(dataset) * 0.7)
#
# 70% train, 30% test
#
train, test = dataset[:cutpoint], dataset[cutpoint:]
print("Len train={0}, len test={1}".format(len(train), len(test)))
# sliding window matrices (npast = window width); dim = n - npast - 1
npast = 3
trainX, trainY = compute_windows(train, npast)
testX, testY   = compute_windows(test, npast)
# should get also the last npred of train
# Multilayer Perceptron model
model = Sequential()
n_hidden = 8
n_output = 1

model.add(Dense(n_hidden, input_dim=npast, activation='relu'))
# hidden neurons, 1 layer
model.add(Dense(n_output))
# output neurons
model.compile(loss='mean_squared_error', optimizer='adam')
model.fit(trainX, trainY, epochs=200, batch_size=10, verbose=2) 
trainScore = model.evaluate(trainX, trainY, verbose=0)
print('Score on train: MSE = {0:0.2f} '.format(trainScore))
testScore = model.evaluate(testX, testY, verbose=0)
print('Score on test:  MSE = {0:0.2f} '.format(testScore))
trainPredict = model.predict(trainX)    # predictions
testForecast = model.predict(testX)     # forecast
plt.rcParams["figure.figsize"] = (10,8) # redefines figure size
plt.plot(dataset)
plt.plot(np.concatenate((np.full(1,np.nan),trainPredict[:,0])))
plt.plot(np.concatenate((np.full(len(train)+1,np.nan), testForecast[:,0])

plt.show()