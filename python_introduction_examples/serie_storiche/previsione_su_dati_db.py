# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 10:47:34 2019

@author: Enrico
"""

import yaml
import pandas as pd, numpy as np, os
import matplotlib.pyplot as plt
import pmdarima as arima
from statsmodels.tsa.stattools import acf
# data upload
with open("../config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.Loader)

os.chdir(r''+cfg['path'])

df = pd.read_csv('./serie_storiche/result_set.csv', header=0)
df["period"] = df["time"].map(str)
df.set_index('period')
aSales =  df['quant'].to_numpy()
# array of sales data
logdata = np.log(aSales)
# log transform
data = pd.Series(logdata)
# convert to pandas series
plt.rcParams["figure.figsize"] = (10,8) # redefines figure size
plt.plot(data.values)
plt.show()
# data plot

# acf plot, industrial
import statsmodels.api as sm
sm.graphics.tsa.plot_acf(data.values, lags=25)
plt.show 
# train and test set
train = data[:-12]
test  = data[-12:] # simple reconstruction, not necessary, unused
reconstruct = np.exp(np.r_[train,test])

# auto arima
model = arima.auto_arima(train.values, start_p=1, start_q=1,test='adf', max_p=3,
                      max_q=3, m=12,start_P=0, seasonal=True,d=None, D=1,
                      trace=True,error_action='ignore', suppress_warnings=True,
                      stepwise=True) # False full grid
print(model.summary())
morder = model.order;
print("Sarimax order {0}".format(morder))
mseasorder = model.seasonal_order;
print("Sarimax seasonal order {0}".format(mseasorder))