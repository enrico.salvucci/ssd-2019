# -*- coding: utf-8 -*-
"""
Created on Wed Oct 30 16:21:38 2019

@author: Enrico
"""

import yaml
import os
from pandas import Series
from matplotlib import pyplot as plot
from statsmodels.tsa.seasonal import seasonal_decompose

# Rename config-example.yml into config.yml
# Also change "path" into the path of your csv file (traffico16.csv)
with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.Loader)

os.chdir(r''+cfg['path'])
# os.chdir('C:/Users/Enrico/source/repos/SSD19/python_introduction_examples/serie_storiche')

plot.rcParams['figure.figsize'] = (10.0, 6.0)
series = Series.from_csv('BoxJenkins.csv', header=0)
result = seasonal_decompose(series, model='multiplicative')
result.plot()
plot.show()