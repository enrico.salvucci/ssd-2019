#import yaml
#import os
#import pandas as pd, matplotlib.pyplot as plt
#from statsmodels.tsa.arima_model import ARIMA
## Import data
#with open("../config.yml", 'r') as ymlfile:
#    cfg = yaml.load(ymlfile, Loader=yaml.Loader)
#
#os.chdir(r''+cfg['path'])
#
#df = pd.read_csv('./serie_storiche/rawAirlinesPassengers.csv', usecols=[0], names=['value'], header=0)
## 1,1,2 ARIMA Model (p,d,q)
#model = ARIMA(df.value, order=(1,1,2))
#model_fit = model.fit(disp=0)
#print(model_fit.summary())
#
## Plot residual errors
#residuals = pd.DataFrame(model_fit.resid)
#fig, ax = plt.subplots(1,2)
#residuals.plot(title="Residuals", ax=ax[0])
#residuals.plot(kind='kde', title='Density', ax=ax[1])
#
#plt.show()

import yaml
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os

with open("../config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.Loader)

os.chdir(r''+cfg['path'])

df = pd.read_csv('./serie_storiche/esempio.csv')
ds = df.sales
from statsmodels.tsa.statespace.sarimax import SARIMAX
sarima_model = SARIMAX(ds, order=(0,2,2), seasonal_order=(0,1,0,4))
sfit = sarima_model.fit()
sfit.plot_diagnostics(figsize=(10, 6))
plt.show()

ypred = sfit.predict(start=0,end=len(ds))
plt.plot(ds.values)
plt.plot(ypred)
plt.xlabel('time');
plt.ylabel('sales')
plt.show()

forewrap = sfit.get_forecast(steps=4)
forecast_ci  = forewrap.conf_int()
forecast_val = forewrap.predicted_meanplt.plot(ds.values)
plt.fill_between(forecast_ci.index,forecast_ci.iloc[:, 0],forecast_ci.iloc[:, 1], color='k', alpha=.25)
plt.plot(forecast_val)
plt.xlabel('time');
plt.ylabel('sales')
plt.show()