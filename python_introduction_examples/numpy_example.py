import numpy as np
# Per installare numpy
# Dal prompt di conda lanciare il comando
# conda install numpy
a = [1, 2, 3]
b = np.array([[1,2,3],[4,5,6],[7,8,9]])
#Vertical columns
c1 = b[:,0];
c2 = b[:,1];
c3 = b[:,2]

print(c1, c2, c3)

names = ["Almeno funziona..", "ma sta GUI?"]
for i in range(len(names)) :
    print("La Peeech dice: '" + names[i] + "'")
    
for name in names :
    print("La Peeech dice: '" + names[i] + "'")
    
    
# Crivello di Eratostene    
# Il crivello di Eratostene è un antico algoritmo per il calcolo delle
# tabelle di numeri primi fino a un certo numero n prefissato    
numbers = [] 

# Popolo la lista
for i in range(1, 100):
    numbers.append(i)

# Rimuovo i numeri che sono multipli di altri
for i in numbers :
    for j in numbers:
        if (i != j and j % i == 0) :
            numbers.remove(j)

print(numbers)

# Esempio di funzione
def matchPrimeNumberIn(list, primeNumber):
    return primeNumber in list

print(matchPrimeNumberIn(numbers, 22)) # False
print(matchPrimeNumberIn(numbers, 11)) # True