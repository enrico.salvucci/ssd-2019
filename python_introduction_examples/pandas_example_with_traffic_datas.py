# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 16:14:27 2019

@author: Enrico
"""

import yaml

import numpy as np
import pandas as pd
from scipy import stats
# to be used later
import matplotlib.pyplot as plt
import os

# Rename config-example.yml into config.yml
# Also change "path" into the path of your csv file (traffico16.csv)
with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.Loader)

os.chdir(r''+cfg['path'])

df = pd.read_csv('traffico16.csv') # dataframe (series)
npa = df['ago-01'].to_numpy()        # numpy array
plt.hist(npa, bins=10, color='#00AA00', edgecolor='black')
plt.title(df.columns[0]);
plt.xlabel('num')
plt.ylabel('days')
plt.show()

# Stampo media, dev standard, minimo, quartili e massimo
print(df['ago-02'].describe())

# Stampo il boxplot
df.boxplot(column = ['ago-01', 'ago-02', 'set-01', 'set-02', 'ott-01', 'ott-02'])
plt.boxplot(npa)