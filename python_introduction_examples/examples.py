# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
a = "ciao"
print(a)
b = a + "ciao"
print(b)

list = [0, 1 , 2, 3, 4, 5, 6, 7, 8, 9]
print(list[0])
print(list[-1])
print(list[2:4])
print(list[2:])

bidimensionalList = [[0, 1, 2, 3, 4], [5, 6, 7, 8, 9]]
# Stampo tutti i valori della seconda dimensione di bidimensionalList
print(bidimensionalList[1][:])

# I'm read-only!
fruitsTuple = ("banana", "cherry", "orange")

# Creo una lista monodimensionale
n = 5
arr = [0] * n
arr = [0 for i in range(n)] # Inizializzo a 0 i primi 5 valori della lista
print(arr)

# Creo una lista monodimensionale
rows = (5, 5)
cols = (5, 5)
arr = [[0]*cols]*rows
arr = [[0 for i in range(cols)] for j in range(rows)]

import numpy as np
b = np.array([[1,2,3],[4,5,6],[7,8,9]])
#Vertical columns
c1 = b[:,0];
c2 = b[:,1];
c3 = b[:,2]

