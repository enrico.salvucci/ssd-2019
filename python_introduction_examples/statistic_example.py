# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 09:27:31 2019

@author: Enrico
"""
import numpy as np
from scipy import stats

values = [113, 124, 124, 132, 146, 151, 170]
mean = np.mean(values)
mode = stats.mode(values)
median = np.median(values)

print(mean)
print(mode.mode)
print(median)
